import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { CoopnationalComponentsPage, CoopnationalDeleteDialog, CoopnationalUpdatePage } from './coopnational.page-object';

const expect = chai.expect;

describe('Coopnational e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let coopnationalComponentsPage: CoopnationalComponentsPage;
  let coopnationalUpdatePage: CoopnationalUpdatePage;
  let coopnationalDeleteDialog: CoopnationalDeleteDialog;
  const username = process.env.E2E_USERNAME ?? 'admin';
  const password = process.env.E2E_PASSWORD ?? 'admin';

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing(username, password);
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Coopnationals', async () => {
    await navBarPage.goToEntity('coopnational');
    coopnationalComponentsPage = new CoopnationalComponentsPage();
    await browser.wait(ec.visibilityOf(coopnationalComponentsPage.title), 5000);
    expect(await coopnationalComponentsPage.getTitle()).to.eq('myblogApp.coopnational.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(coopnationalComponentsPage.entities), ec.visibilityOf(coopnationalComponentsPage.noResult)),
      1000
    );
  });

  it('should load create Coopnational page', async () => {
    await coopnationalComponentsPage.clickOnCreateButton();
    coopnationalUpdatePage = new CoopnationalUpdatePage();
    expect(await coopnationalUpdatePage.getPageTitle()).to.eq('myblogApp.coopnational.home.createOrEditLabel');
    await coopnationalUpdatePage.cancel();
  });

  it('should create and save Coopnationals', async () => {
    const nbButtonsBeforeCreate = await coopnationalComponentsPage.countDeleteButtons();

    await coopnationalComponentsPage.clickOnCreateButton();

    await promise.all([coopnationalUpdatePage.setNameInput('name'), coopnationalUpdatePage.cooplocalSelectLastOption()]);

    expect(await coopnationalUpdatePage.getNameInput()).to.eq('name', 'Expected Name value to be equals to name');

    await coopnationalUpdatePage.save();
    expect(await coopnationalUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await coopnationalComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Coopnational', async () => {
    const nbButtonsBeforeDelete = await coopnationalComponentsPage.countDeleteButtons();
    await coopnationalComponentsPage.clickOnLastDeleteButton();

    coopnationalDeleteDialog = new CoopnationalDeleteDialog();
    expect(await coopnationalDeleteDialog.getDialogTitle()).to.eq('myblogApp.coopnational.delete.question');
    await coopnationalDeleteDialog.clickOnConfirmButton();
    await browser.wait(ec.visibilityOf(coopnationalComponentsPage.title), 5000);

    expect(await coopnationalComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
