package fr.polytech.info4.domain;

import static org.assertj.core.api.Assertions.assertThat;

import fr.polytech.info4.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class CoopnationalTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Coopnational.class);
        Coopnational coopnational1 = new Coopnational();
        coopnational1.setId("id1");
        Coopnational coopnational2 = new Coopnational();
        coopnational2.setId(coopnational1.getId());
        assertThat(coopnational1).isEqualTo(coopnational2);
        coopnational2.setId("id2");
        assertThat(coopnational1).isNotEqualTo(coopnational2);
        coopnational1.setId(null);
        assertThat(coopnational1).isNotEqualTo(coopnational2);
    }
}
