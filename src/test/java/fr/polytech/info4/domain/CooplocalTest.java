package fr.polytech.info4.domain;

import static org.assertj.core.api.Assertions.assertThat;

import fr.polytech.info4.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class CooplocalTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Cooplocal.class);
        Cooplocal cooplocal1 = new Cooplocal();
        cooplocal1.setId("id1");
        Cooplocal cooplocal2 = new Cooplocal();
        cooplocal2.setId(cooplocal1.getId());
        assertThat(cooplocal1).isEqualTo(cooplocal2);
        cooplocal2.setId("id2");
        assertThat(cooplocal1).isNotEqualTo(cooplocal2);
        cooplocal1.setId(null);
        assertThat(cooplocal1).isNotEqualTo(cooplocal2);
    }
}
