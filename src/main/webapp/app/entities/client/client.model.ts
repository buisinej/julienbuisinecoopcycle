import { ICommande } from 'app/entities/commande/commande.model';

export interface IClient {
  id?: number;
  adresse?: string | null;
  name?: string;
  commandes?: ICommande[] | null;
}

export class Client implements IClient {
  constructor(public id?: number, public adresse?: string | null, public name?: string, public commandes?: ICommande[] | null) {}
}

export function getClientIdentifier(client: IClient): number | undefined {
  return client.id;
}
