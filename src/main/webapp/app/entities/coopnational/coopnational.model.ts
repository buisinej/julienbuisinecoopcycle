import { ICooplocal } from 'app/entities/cooplocal/cooplocal.model';

export interface ICoopnational {
  id?: string;
  name?: string;
  cooplocal?: ICooplocal | null;
}

export class Coopnational implements ICoopnational {
  constructor(public id?: string, public name?: string, public cooplocal?: ICooplocal | null) {}
}

export function getCoopnationalIdentifier(coopnational: ICoopnational): string | undefined {
  return coopnational.id;
}
