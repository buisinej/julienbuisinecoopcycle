import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ICoopnational, Coopnational } from '../coopnational.model';

import { CoopnationalService } from './coopnational.service';

describe('Service Tests', () => {
  describe('Coopnational Service', () => {
    let service: CoopnationalService;
    let httpMock: HttpTestingController;
    let elemDefault: ICoopnational;
    let expectedResult: ICoopnational | ICoopnational[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(CoopnationalService);
      httpMock = TestBed.inject(HttpTestingController);

      elemDefault = {
        id: 'AAAAAAA',
        name: 'AAAAAAA',
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find('ABC').subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a Coopnational', () => {
        const returnedFromService = Object.assign(
          {
            id: 'ID',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new Coopnational()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a Coopnational', () => {
        const returnedFromService = Object.assign(
          {
            id: 'BBBBBB',
            name: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a Coopnational', () => {
        const patchObject = Object.assign(
          {
            name: 'BBBBBB',
          },
          new Coopnational()
        );

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign({}, returnedFromService);

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of Coopnational', () => {
        const returnedFromService = Object.assign(
          {
            id: 'BBBBBB',
            name: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Coopnational', () => {
        service.delete('ABC').subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addCoopnationalToCollectionIfMissing', () => {
        it('should add a Coopnational to an empty array', () => {
          const coopnational: ICoopnational = { id: 'ABC' };
          expectedResult = service.addCoopnationalToCollectionIfMissing([], coopnational);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(coopnational);
        });

        it('should not add a Coopnational to an array that contains it', () => {
          const coopnational: ICoopnational = { id: 'ABC' };
          const coopnationalCollection: ICoopnational[] = [
            {
              ...coopnational,
            },
            { id: 'CBA' },
          ];
          expectedResult = service.addCoopnationalToCollectionIfMissing(coopnationalCollection, coopnational);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a Coopnational to an array that doesn't contain it", () => {
          const coopnational: ICoopnational = { id: 'ABC' };
          const coopnationalCollection: ICoopnational[] = [{ id: 'CBA' }];
          expectedResult = service.addCoopnationalToCollectionIfMissing(coopnationalCollection, coopnational);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(coopnational);
        });

        it('should add only unique Coopnational to an array', () => {
          const coopnationalArray: ICoopnational[] = [{ id: 'ABC' }, { id: 'CBA' }, { id: 'attitude-oriented' }];
          const coopnationalCollection: ICoopnational[] = [{ id: 'ABC' }];
          expectedResult = service.addCoopnationalToCollectionIfMissing(coopnationalCollection, ...coopnationalArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const coopnational: ICoopnational = { id: 'ABC' };
          const coopnational2: ICoopnational = { id: 'CBA' };
          expectedResult = service.addCoopnationalToCollectionIfMissing([], coopnational, coopnational2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(coopnational);
          expect(expectedResult).toContain(coopnational2);
        });

        it('should accept null and undefined values', () => {
          const coopnational: ICoopnational = { id: 'ABC' };
          expectedResult = service.addCoopnationalToCollectionIfMissing([], null, coopnational, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(coopnational);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
