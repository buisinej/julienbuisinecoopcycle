import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ICoopnational, getCoopnationalIdentifier } from '../coopnational.model';

export type EntityResponseType = HttpResponse<ICoopnational>;
export type EntityArrayResponseType = HttpResponse<ICoopnational[]>;

@Injectable({ providedIn: 'root' })
export class CoopnationalService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('api/coopnationals');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(coopnational: ICoopnational): Observable<EntityResponseType> {
    return this.http.post<ICoopnational>(this.resourceUrl, coopnational, { observe: 'response' });
  }

  update(coopnational: ICoopnational): Observable<EntityResponseType> {
    return this.http.put<ICoopnational>(`${this.resourceUrl}/${getCoopnationalIdentifier(coopnational) as string}`, coopnational, {
      observe: 'response',
    });
  }

  partialUpdate(coopnational: ICoopnational): Observable<EntityResponseType> {
    return this.http.patch<ICoopnational>(`${this.resourceUrl}/${getCoopnationalIdentifier(coopnational) as string}`, coopnational, {
      observe: 'response',
    });
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http.get<ICoopnational>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICoopnational[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addCoopnationalToCollectionIfMissing(
    coopnationalCollection: ICoopnational[],
    ...coopnationalsToCheck: (ICoopnational | null | undefined)[]
  ): ICoopnational[] {
    const coopnationals: ICoopnational[] = coopnationalsToCheck.filter(isPresent);
    if (coopnationals.length > 0) {
      const coopnationalCollectionIdentifiers = coopnationalCollection.map(
        coopnationalItem => getCoopnationalIdentifier(coopnationalItem)!
      );
      const coopnationalsToAdd = coopnationals.filter(coopnationalItem => {
        const coopnationalIdentifier = getCoopnationalIdentifier(coopnationalItem);
        if (coopnationalIdentifier == null || coopnationalCollectionIdentifiers.includes(coopnationalIdentifier)) {
          return false;
        }
        coopnationalCollectionIdentifiers.push(coopnationalIdentifier);
        return true;
      });
      return [...coopnationalsToAdd, ...coopnationalCollection];
    }
    return coopnationalCollection;
  }
}
