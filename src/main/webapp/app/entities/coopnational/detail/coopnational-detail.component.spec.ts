import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { CoopnationalDetailComponent } from './coopnational-detail.component';

describe('Component Tests', () => {
  describe('Coopnational Management Detail Component', () => {
    let comp: CoopnationalDetailComponent;
    let fixture: ComponentFixture<CoopnationalDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [CoopnationalDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ coopnational: { id: 'ABC' } }) },
          },
        ],
      })
        .overrideTemplate(CoopnationalDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CoopnationalDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load coopnational on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.coopnational).toEqual(jasmine.objectContaining({ id: 'ABC' }));
      });
    });
  });
});
