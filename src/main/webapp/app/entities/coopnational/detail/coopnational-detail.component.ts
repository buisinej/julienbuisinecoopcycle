import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICoopnational } from '../coopnational.model';

@Component({
  selector: 'jhi-coopnational-detail',
  templateUrl: './coopnational-detail.component.html',
})
export class CoopnationalDetailComponent implements OnInit {
  coopnational: ICoopnational | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ coopnational }) => {
      this.coopnational = coopnational;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
