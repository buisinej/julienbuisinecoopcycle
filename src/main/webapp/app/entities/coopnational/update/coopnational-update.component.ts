import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { ICoopnational, Coopnational } from '../coopnational.model';
import { CoopnationalService } from '../service/coopnational.service';
import { ICooplocal } from 'app/entities/cooplocal/cooplocal.model';
import { CooplocalService } from 'app/entities/cooplocal/service/cooplocal.service';

@Component({
  selector: 'jhi-coopnational-update',
  templateUrl: './coopnational-update.component.html',
})
export class CoopnationalUpdateComponent implements OnInit {
  isSaving = false;

  cooplocalsSharedCollection: ICooplocal[] = [];

  editForm = this.fb.group({
    id: [null, [Validators.required]],
    name: [null, [Validators.required]],
    cooplocal: [],
  });

  constructor(
    protected coopnationalService: CoopnationalService,
    protected cooplocalService: CooplocalService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ coopnational }) => {
      this.updateForm(coopnational);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const coopnational = this.createFromForm();
    if (coopnational.id !== undefined) {
      this.subscribeToSaveResponse(this.coopnationalService.update(coopnational));
    } else {
      this.subscribeToSaveResponse(this.coopnationalService.create(coopnational));
    }
  }

  trackCooplocalById(index: number, item: ICooplocal): string {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICoopnational>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(coopnational: ICoopnational): void {
    this.editForm.patchValue({
      id: coopnational.id,
      name: coopnational.name,
      cooplocal: coopnational.cooplocal,
    });

    this.cooplocalsSharedCollection = this.cooplocalService.addCooplocalToCollectionIfMissing(
      this.cooplocalsSharedCollection,
      coopnational.cooplocal
    );
  }

  protected loadRelationshipsOptions(): void {
    this.cooplocalService
      .query()
      .pipe(map((res: HttpResponse<ICooplocal[]>) => res.body ?? []))
      .pipe(
        map((cooplocals: ICooplocal[]) =>
          this.cooplocalService.addCooplocalToCollectionIfMissing(cooplocals, this.editForm.get('cooplocal')!.value)
        )
      )
      .subscribe((cooplocals: ICooplocal[]) => (this.cooplocalsSharedCollection = cooplocals));
  }

  protected createFromForm(): ICoopnational {
    return {
      ...new Coopnational(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      cooplocal: this.editForm.get(['cooplocal'])!.value,
    };
  }
}
