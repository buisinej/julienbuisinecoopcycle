jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { ICoopnational, Coopnational } from '../coopnational.model';
import { CoopnationalService } from '../service/coopnational.service';

import { CoopnationalRoutingResolveService } from './coopnational-routing-resolve.service';

describe('Service Tests', () => {
  describe('Coopnational routing resolve service', () => {
    let mockRouter: Router;
    let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
    let routingResolveService: CoopnationalRoutingResolveService;
    let service: CoopnationalService;
    let resultCoopnational: ICoopnational | undefined;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [Router, ActivatedRouteSnapshot],
      });
      mockRouter = TestBed.inject(Router);
      mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
      routingResolveService = TestBed.inject(CoopnationalRoutingResolveService);
      service = TestBed.inject(CoopnationalService);
      resultCoopnational = undefined;
    });

    describe('resolve', () => {
      it('should return ICoopnational returned by find', () => {
        // GIVEN
        service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
        mockActivatedRouteSnapshot.params = { id: 'ABC' };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultCoopnational = result;
        });

        // THEN
        expect(service.find).toBeCalledWith('ABC');
        expect(resultCoopnational).toEqual({ id: 'ABC' });
      });

      it('should return new ICoopnational if id is not provided', () => {
        // GIVEN
        service.find = jest.fn();
        mockActivatedRouteSnapshot.params = {};

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultCoopnational = result;
        });

        // THEN
        expect(service.find).not.toBeCalled();
        expect(resultCoopnational).toEqual(new Coopnational());
      });

      it('should route to 404 page if data not found in server', () => {
        // GIVEN
        spyOn(service, 'find').and.returnValue(of(new HttpResponse({ body: null })));
        mockActivatedRouteSnapshot.params = { id: 'ABC' };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultCoopnational = result;
        });

        // THEN
        expect(service.find).toBeCalledWith('ABC');
        expect(resultCoopnational).toEqual(undefined);
        expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
      });
    });
  });
});
