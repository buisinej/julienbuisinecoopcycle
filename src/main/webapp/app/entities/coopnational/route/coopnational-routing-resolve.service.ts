import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { ICoopnational, Coopnational } from '../coopnational.model';
import { CoopnationalService } from '../service/coopnational.service';

@Injectable({ providedIn: 'root' })
export class CoopnationalRoutingResolveService implements Resolve<ICoopnational> {
  constructor(protected service: CoopnationalService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICoopnational> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((coopnational: HttpResponse<Coopnational>) => {
          if (coopnational.body) {
            return of(coopnational.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Coopnational());
  }
}
