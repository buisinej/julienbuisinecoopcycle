import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ICoopnational } from '../coopnational.model';
import { CoopnationalService } from '../service/coopnational.service';

@Component({
  templateUrl: './coopnational-delete-dialog.component.html',
})
export class CoopnationalDeleteDialogComponent {
  coopnational?: ICoopnational;

  constructor(protected coopnationalService: CoopnationalService, public activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.coopnationalService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
