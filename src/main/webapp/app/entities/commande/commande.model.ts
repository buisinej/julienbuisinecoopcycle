import * as dayjs from 'dayjs';
import { IClient } from 'app/entities/client/client.model';
import { ICourse } from 'app/entities/course/course.model';
import { IRestaurant } from 'app/entities/restaurant/restaurant.model';
import { IPlat } from 'app/entities/plat/plat.model';

export interface ICommande {
  id?: number;
  date?: dayjs.Dayjs;
  prix?: number;
  client?: IClient | null;
  course?: ICourse | null;
  restaurant?: IRestaurant | null;
  plat?: IPlat | null;
}

export class Commande implements ICommande {
  constructor(
    public id?: number,
    public date?: dayjs.Dayjs,
    public prix?: number,
    public client?: IClient | null,
    public course?: ICourse | null,
    public restaurant?: IRestaurant | null,
    public plat?: IPlat | null
  ) {}
}

export function getCommandeIdentifier(commande: ICommande): number | undefined {
  return commande.id;
}
