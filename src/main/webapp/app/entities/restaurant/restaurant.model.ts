import { IPlat } from 'app/entities/plat/plat.model';
import { ICommande } from 'app/entities/commande/commande.model';
import { ICooplocal } from 'app/entities/cooplocal/cooplocal.model';

export interface IRestaurant {
  id?: number;
  name?: string;
  plats?: IPlat[] | null;
  commandes?: ICommande[] | null;
  cooplocals?: ICooplocal[] | null;
}

export class Restaurant implements IRestaurant {
  constructor(
    public id?: number,
    public name?: string,
    public plats?: IPlat[] | null,
    public commandes?: ICommande[] | null,
    public cooplocals?: ICooplocal[] | null
  ) {}
}

export function getRestaurantIdentifier(restaurant: IRestaurant): number | undefined {
  return restaurant.id;
}
