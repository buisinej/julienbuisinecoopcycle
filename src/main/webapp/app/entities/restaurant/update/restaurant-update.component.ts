import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IRestaurant, Restaurant } from '../restaurant.model';
import { RestaurantService } from '../service/restaurant.service';
import { ICooplocal } from 'app/entities/cooplocal/cooplocal.model';
import { CooplocalService } from 'app/entities/cooplocal/service/cooplocal.service';

@Component({
  selector: 'jhi-restaurant-update',
  templateUrl: './restaurant-update.component.html',
})
export class RestaurantUpdateComponent implements OnInit {
  isSaving = false;

  cooplocalsSharedCollection: ICooplocal[] = [];

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required]],
    cooplocals: [],
  });

  constructor(
    protected restaurantService: RestaurantService,
    protected cooplocalService: CooplocalService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ restaurant }) => {
      this.updateForm(restaurant);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const restaurant = this.createFromForm();
    if (restaurant.id !== undefined) {
      this.subscribeToSaveResponse(this.restaurantService.update(restaurant));
    } else {
      this.subscribeToSaveResponse(this.restaurantService.create(restaurant));
    }
  }

  trackCooplocalById(index: number, item: ICooplocal): string {
    return item.id!;
  }

  getSelectedCooplocal(option: ICooplocal, selectedVals?: ICooplocal[]): ICooplocal {
    if (selectedVals) {
      for (const selectedVal of selectedVals) {
        if (option.id === selectedVal.id) {
          return selectedVal;
        }
      }
    }
    return option;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IRestaurant>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(restaurant: IRestaurant): void {
    this.editForm.patchValue({
      id: restaurant.id,
      name: restaurant.name,
      cooplocals: restaurant.cooplocals,
    });

    this.cooplocalsSharedCollection = this.cooplocalService.addCooplocalToCollectionIfMissing(
      this.cooplocalsSharedCollection,
      ...(restaurant.cooplocals ?? [])
    );
  }

  protected loadRelationshipsOptions(): void {
    this.cooplocalService
      .query()
      .pipe(map((res: HttpResponse<ICooplocal[]>) => res.body ?? []))
      .pipe(
        map((cooplocals: ICooplocal[]) =>
          this.cooplocalService.addCooplocalToCollectionIfMissing(cooplocals, ...(this.editForm.get('cooplocals')!.value ?? []))
        )
      )
      .subscribe((cooplocals: ICooplocal[]) => (this.cooplocalsSharedCollection = cooplocals));
  }

  protected createFromForm(): IRestaurant {
    return {
      ...new Restaurant(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      cooplocals: this.editForm.get(['cooplocals'])!.value,
    };
  }
}
