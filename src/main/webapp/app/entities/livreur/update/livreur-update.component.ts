import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { ILivreur, Livreur } from '../livreur.model';
import { LivreurService } from '../service/livreur.service';
import { ICooplocal } from 'app/entities/cooplocal/cooplocal.model';
import { CooplocalService } from 'app/entities/cooplocal/service/cooplocal.service';

@Component({
  selector: 'jhi-livreur-update',
  templateUrl: './livreur-update.component.html',
})
export class LivreurUpdateComponent implements OnInit {
  isSaving = false;

  cooplocalsSharedCollection: ICooplocal[] = [];

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required]],
    cooplocal: [],
  });

  constructor(
    protected livreurService: LivreurService,
    protected cooplocalService: CooplocalService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ livreur }) => {
      this.updateForm(livreur);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const livreur = this.createFromForm();
    if (livreur.id !== undefined) {
      this.subscribeToSaveResponse(this.livreurService.update(livreur));
    } else {
      this.subscribeToSaveResponse(this.livreurService.create(livreur));
    }
  }

  trackCooplocalById(index: number, item: ICooplocal): string {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ILivreur>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(livreur: ILivreur): void {
    this.editForm.patchValue({
      id: livreur.id,
      name: livreur.name,
      cooplocal: livreur.cooplocal,
    });

    this.cooplocalsSharedCollection = this.cooplocalService.addCooplocalToCollectionIfMissing(
      this.cooplocalsSharedCollection,
      livreur.cooplocal
    );
  }

  protected loadRelationshipsOptions(): void {
    this.cooplocalService
      .query()
      .pipe(map((res: HttpResponse<ICooplocal[]>) => res.body ?? []))
      .pipe(
        map((cooplocals: ICooplocal[]) =>
          this.cooplocalService.addCooplocalToCollectionIfMissing(cooplocals, this.editForm.get('cooplocal')!.value)
        )
      )
      .subscribe((cooplocals: ICooplocal[]) => (this.cooplocalsSharedCollection = cooplocals));
  }

  protected createFromForm(): ILivreur {
    return {
      ...new Livreur(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      cooplocal: this.editForm.get(['cooplocal'])!.value,
    };
  }
}
