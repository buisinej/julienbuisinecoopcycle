jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { CooplocalService } from '../service/cooplocal.service';
import { ICooplocal, Cooplocal } from '../cooplocal.model';

import { CooplocalUpdateComponent } from './cooplocal-update.component';

describe('Component Tests', () => {
  describe('Cooplocal Management Update Component', () => {
    let comp: CooplocalUpdateComponent;
    let fixture: ComponentFixture<CooplocalUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let cooplocalService: CooplocalService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [CooplocalUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(CooplocalUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CooplocalUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      cooplocalService = TestBed.inject(CooplocalService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should update editForm', () => {
        const cooplocal: ICooplocal = { id: 'CBA' };

        activatedRoute.data = of({ cooplocal });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(cooplocal));
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const cooplocal = { id: 'ABC' };
        spyOn(cooplocalService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ cooplocal });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: cooplocal }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(cooplocalService.update).toHaveBeenCalledWith(cooplocal);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const cooplocal = new Cooplocal();
        spyOn(cooplocalService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ cooplocal });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: cooplocal }));
        saveSubject.complete();

        // THEN
        expect(cooplocalService.create).toHaveBeenCalledWith(cooplocal);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const cooplocal = { id: 'ABC' };
        spyOn(cooplocalService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ cooplocal });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(cooplocalService.update).toHaveBeenCalledWith(cooplocal);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });
  });
});
