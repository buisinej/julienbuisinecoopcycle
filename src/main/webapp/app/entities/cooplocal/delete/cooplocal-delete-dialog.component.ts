import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ICooplocal } from '../cooplocal.model';
import { CooplocalService } from '../service/cooplocal.service';

@Component({
  templateUrl: './cooplocal-delete-dialog.component.html',
})
export class CooplocalDeleteDialogComponent {
  cooplocal?: ICooplocal;

  constructor(protected cooplocalService: CooplocalService, public activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.cooplocalService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
