import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { CooplocalComponent } from '../list/cooplocal.component';
import { CooplocalDetailComponent } from '../detail/cooplocal-detail.component';
import { CooplocalUpdateComponent } from '../update/cooplocal-update.component';
import { CooplocalRoutingResolveService } from './cooplocal-routing-resolve.service';

const cooplocalRoute: Routes = [
  {
    path: '',
    component: CooplocalComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: CooplocalDetailComponent,
    resolve: {
      cooplocal: CooplocalRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: CooplocalUpdateComponent,
    resolve: {
      cooplocal: CooplocalRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: CooplocalUpdateComponent,
    resolve: {
      cooplocal: CooplocalRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(cooplocalRoute)],
  exports: [RouterModule],
})
export class CooplocalRoutingModule {}
