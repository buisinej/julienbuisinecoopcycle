import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { ICooplocal, Cooplocal } from '../cooplocal.model';
import { CooplocalService } from '../service/cooplocal.service';

@Injectable({ providedIn: 'root' })
export class CooplocalRoutingResolveService implements Resolve<ICooplocal> {
  constructor(protected service: CooplocalService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICooplocal> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((cooplocal: HttpResponse<Cooplocal>) => {
          if (cooplocal.body) {
            return of(cooplocal.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Cooplocal());
  }
}
