import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { CooplocalService } from '../service/cooplocal.service';

import { CooplocalComponent } from './cooplocal.component';

describe('Component Tests', () => {
  describe('Cooplocal Management Component', () => {
    let comp: CooplocalComponent;
    let fixture: ComponentFixture<CooplocalComponent>;
    let service: CooplocalService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [CooplocalComponent],
      })
        .overrideTemplate(CooplocalComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CooplocalComponent);
      comp = fixture.componentInstance;
      service = TestBed.inject(CooplocalService);

      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [{ id: 'ABC' }],
            headers,
          })
        )
      );
    });

    it('Should call load all on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.cooplocals?.[0]).toEqual(jasmine.objectContaining({ id: 'ABC' }));
    });
  });
});
