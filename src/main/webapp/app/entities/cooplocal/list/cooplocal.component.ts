import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ICooplocal } from '../cooplocal.model';
import { CooplocalService } from '../service/cooplocal.service';
import { CooplocalDeleteDialogComponent } from '../delete/cooplocal-delete-dialog.component';

@Component({
  selector: 'jhi-cooplocal',
  templateUrl: './cooplocal.component.html',
})
export class CooplocalComponent implements OnInit {
  cooplocals?: ICooplocal[];
  isLoading = false;

  constructor(protected cooplocalService: CooplocalService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.cooplocalService.query().subscribe(
      (res: HttpResponse<ICooplocal[]>) => {
        this.isLoading = false;
        this.cooplocals = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: ICooplocal): string {
    return item.id!;
  }

  delete(cooplocal: ICooplocal): void {
    const modalRef = this.modalService.open(CooplocalDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.cooplocal = cooplocal;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
