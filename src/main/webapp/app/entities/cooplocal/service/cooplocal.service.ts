import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ICooplocal, getCooplocalIdentifier } from '../cooplocal.model';

export type EntityResponseType = HttpResponse<ICooplocal>;
export type EntityArrayResponseType = HttpResponse<ICooplocal[]>;

@Injectable({ providedIn: 'root' })
export class CooplocalService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('api/cooplocals');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(cooplocal: ICooplocal): Observable<EntityResponseType> {
    return this.http.post<ICooplocal>(this.resourceUrl, cooplocal, { observe: 'response' });
  }

  update(cooplocal: ICooplocal): Observable<EntityResponseType> {
    return this.http.put<ICooplocal>(`${this.resourceUrl}/${getCooplocalIdentifier(cooplocal) as string}`, cooplocal, {
      observe: 'response',
    });
  }

  partialUpdate(cooplocal: ICooplocal): Observable<EntityResponseType> {
    return this.http.patch<ICooplocal>(`${this.resourceUrl}/${getCooplocalIdentifier(cooplocal) as string}`, cooplocal, {
      observe: 'response',
    });
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http.get<ICooplocal>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICooplocal[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addCooplocalToCollectionIfMissing(
    cooplocalCollection: ICooplocal[],
    ...cooplocalsToCheck: (ICooplocal | null | undefined)[]
  ): ICooplocal[] {
    const cooplocals: ICooplocal[] = cooplocalsToCheck.filter(isPresent);
    if (cooplocals.length > 0) {
      const cooplocalCollectionIdentifiers = cooplocalCollection.map(cooplocalItem => getCooplocalIdentifier(cooplocalItem)!);
      const cooplocalsToAdd = cooplocals.filter(cooplocalItem => {
        const cooplocalIdentifier = getCooplocalIdentifier(cooplocalItem);
        if (cooplocalIdentifier == null || cooplocalCollectionIdentifiers.includes(cooplocalIdentifier)) {
          return false;
        }
        cooplocalCollectionIdentifiers.push(cooplocalIdentifier);
        return true;
      });
      return [...cooplocalsToAdd, ...cooplocalCollection];
    }
    return cooplocalCollection;
  }
}
