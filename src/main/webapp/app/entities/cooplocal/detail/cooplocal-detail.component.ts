import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICooplocal } from '../cooplocal.model';

@Component({
  selector: 'jhi-cooplocal-detail',
  templateUrl: './cooplocal-detail.component.html',
})
export class CooplocalDetailComponent implements OnInit {
  cooplocal: ICooplocal | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ cooplocal }) => {
      this.cooplocal = cooplocal;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
