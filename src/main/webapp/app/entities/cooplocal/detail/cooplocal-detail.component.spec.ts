import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { CooplocalDetailComponent } from './cooplocal-detail.component';

describe('Component Tests', () => {
  describe('Cooplocal Management Detail Component', () => {
    let comp: CooplocalDetailComponent;
    let fixture: ComponentFixture<CooplocalDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [CooplocalDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ cooplocal: { id: 'ABC' } }) },
          },
        ],
      })
        .overrideTemplate(CooplocalDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CooplocalDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load cooplocal on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.cooplocal).toEqual(jasmine.objectContaining({ id: 'ABC' }));
      });
    });
  });
});
