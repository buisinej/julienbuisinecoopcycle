package fr.polytech.info4.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Coopnational.
 */
@Entity
@Table(name = "coopnational")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Coopnational implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @ManyToOne
    @JsonIgnoreProperties(value = { "coopnationals", "livreurs", "restaurants" }, allowSetters = true)
    private Cooplocal cooplocal;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Coopnational id(String id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return this.name;
    }

    public Coopnational name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Cooplocal getCooplocal() {
        return this.cooplocal;
    }

    public Coopnational cooplocal(Cooplocal cooplocal) {
        this.setCooplocal(cooplocal);
        return this;
    }

    public void setCooplocal(Cooplocal cooplocal) {
        this.cooplocal = cooplocal;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Coopnational)) {
            return false;
        }
        return id != null && id.equals(((Coopnational) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Coopnational{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            "}";
    }
}
