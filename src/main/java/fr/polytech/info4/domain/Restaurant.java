package fr.polytech.info4.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Restaurant.
 */
@Entity
@Table(name = "restaurant")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Restaurant implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @OneToMany(mappedBy = "restaurant")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "commandes", "restaurant" }, allowSetters = true)
    private Set<Plat> plats = new HashSet<>();

    @OneToMany(mappedBy = "restaurant")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "client", "course", "restaurant", "plat" }, allowSetters = true)
    private Set<Commande> commandes = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JoinTable(
        name = "rel_restaurant__cooplocal",
        joinColumns = @JoinColumn(name = "restaurant_id"),
        inverseJoinColumns = @JoinColumn(name = "cooplocal_id")
    )
    @JsonIgnoreProperties(value = { "coopnationals", "livreurs", "restaurants" }, allowSetters = true)
    private Set<Cooplocal> cooplocals = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Restaurant id(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return this.name;
    }

    public Restaurant name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Plat> getPlats() {
        return this.plats;
    }

    public Restaurant plats(Set<Plat> plats) {
        this.setPlats(plats);
        return this;
    }

    public Restaurant addPlat(Plat plat) {
        this.plats.add(plat);
        plat.setRestaurant(this);
        return this;
    }

    public Restaurant removePlat(Plat plat) {
        this.plats.remove(plat);
        plat.setRestaurant(null);
        return this;
    }

    public void setPlats(Set<Plat> plats) {
        if (this.plats != null) {
            this.plats.forEach(i -> i.setRestaurant(null));
        }
        if (plats != null) {
            plats.forEach(i -> i.setRestaurant(this));
        }
        this.plats = plats;
    }

    public Set<Commande> getCommandes() {
        return this.commandes;
    }

    public Restaurant commandes(Set<Commande> commandes) {
        this.setCommandes(commandes);
        return this;
    }

    public Restaurant addCommande(Commande commande) {
        this.commandes.add(commande);
        commande.setRestaurant(this);
        return this;
    }

    public Restaurant removeCommande(Commande commande) {
        this.commandes.remove(commande);
        commande.setRestaurant(null);
        return this;
    }

    public void setCommandes(Set<Commande> commandes) {
        if (this.commandes != null) {
            this.commandes.forEach(i -> i.setRestaurant(null));
        }
        if (commandes != null) {
            commandes.forEach(i -> i.setRestaurant(this));
        }
        this.commandes = commandes;
    }

    public Set<Cooplocal> getCooplocals() {
        return this.cooplocals;
    }

    public Restaurant cooplocals(Set<Cooplocal> cooplocals) {
        this.setCooplocals(cooplocals);
        return this;
    }

    public Restaurant addCooplocal(Cooplocal cooplocal) {
        this.cooplocals.add(cooplocal);
        cooplocal.getRestaurants().add(this);
        return this;
    }

    public Restaurant removeCooplocal(Cooplocal cooplocal) {
        this.cooplocals.remove(cooplocal);
        cooplocal.getRestaurants().remove(this);
        return this;
    }

    public void setCooplocals(Set<Cooplocal> cooplocals) {
        this.cooplocals = cooplocals;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Restaurant)) {
            return false;
        }
        return id != null && id.equals(((Restaurant) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Restaurant{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            "}";
    }
}
