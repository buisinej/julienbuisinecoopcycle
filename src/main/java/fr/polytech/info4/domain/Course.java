package fr.polytech.info4.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Course.
 */
@Entity
@Table(name = "course")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Course implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Min(value = 0)
    @Column(name = "prix", nullable = false)
    private Integer prix;

    @NotNull
    @Column(name = "distance", nullable = false)
    private Float distance;

    @OneToMany(mappedBy = "course")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "client", "course", "restaurant", "plat" }, allowSetters = true)
    private Set<Commande> commandes = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = { "courses", "cooplocal" }, allowSetters = true)
    private Livreur livreur;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Course id(Long id) {
        this.id = id;
        return this;
    }

    public Integer getPrix() {
        return this.prix;
    }

    public Course prix(Integer prix) {
        this.prix = prix;
        return this;
    }

    public void setPrix(Integer prix) {
        this.prix = prix;
    }

    public Float getDistance() {
        return this.distance;
    }

    public Course distance(Float distance) {
        this.distance = distance;
        return this;
    }

    public void setDistance(Float distance) {
        this.distance = distance;
    }

    public Set<Commande> getCommandes() {
        return this.commandes;
    }

    public Course commandes(Set<Commande> commandes) {
        this.setCommandes(commandes);
        return this;
    }

    public Course addCommande(Commande commande) {
        this.commandes.add(commande);
        commande.setCourse(this);
        return this;
    }

    public Course removeCommande(Commande commande) {
        this.commandes.remove(commande);
        commande.setCourse(null);
        return this;
    }

    public void setCommandes(Set<Commande> commandes) {
        if (this.commandes != null) {
            this.commandes.forEach(i -> i.setCourse(null));
        }
        if (commandes != null) {
            commandes.forEach(i -> i.setCourse(this));
        }
        this.commandes = commandes;
    }

    public Livreur getLivreur() {
        return this.livreur;
    }

    public Course livreur(Livreur livreur) {
        this.setLivreur(livreur);
        return this;
    }

    public void setLivreur(Livreur livreur) {
        this.livreur = livreur;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Course)) {
            return false;
        }
        return id != null && id.equals(((Course) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Course{" +
            "id=" + getId() +
            ", prix=" + getPrix() +
            ", distance=" + getDistance() +
            "}";
    }
}
