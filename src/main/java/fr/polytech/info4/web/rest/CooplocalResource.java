package fr.polytech.info4.web.rest;

import fr.polytech.info4.repository.CooplocalRepository;
import fr.polytech.info4.service.CooplocalQueryService;
import fr.polytech.info4.service.CooplocalService;
import fr.polytech.info4.service.criteria.CooplocalCriteria;
import fr.polytech.info4.service.dto.CooplocalDTO;
import fr.polytech.info4.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link fr.polytech.info4.domain.Cooplocal}.
 */
@RestController
@RequestMapping("/api")
public class CooplocalResource {

    private final Logger log = LoggerFactory.getLogger(CooplocalResource.class);

    private static final String ENTITY_NAME = "cooplocal";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CooplocalService cooplocalService;

    private final CooplocalRepository cooplocalRepository;

    private final CooplocalQueryService cooplocalQueryService;

    public CooplocalResource(
        CooplocalService cooplocalService,
        CooplocalRepository cooplocalRepository,
        CooplocalQueryService cooplocalQueryService
    ) {
        this.cooplocalService = cooplocalService;
        this.cooplocalRepository = cooplocalRepository;
        this.cooplocalQueryService = cooplocalQueryService;
    }

    /**
     * {@code POST  /cooplocals} : Create a new cooplocal.
     *
     * @param cooplocalDTO the cooplocalDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new cooplocalDTO, or with status {@code 400 (Bad Request)} if the cooplocal has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/cooplocals")
    public ResponseEntity<CooplocalDTO> createCooplocal(@Valid @RequestBody CooplocalDTO cooplocalDTO) throws URISyntaxException {
        log.debug("REST request to save Cooplocal : {}", cooplocalDTO);
        if (cooplocalDTO.getId() != null) {
            throw new BadRequestAlertException("A new cooplocal cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CooplocalDTO result = cooplocalService.save(cooplocalDTO);
        return ResponseEntity
            .created(new URI("/api/cooplocals/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId()))
            .body(result);
    }

    /**
     * {@code PUT  /cooplocals/:id} : Updates an existing cooplocal.
     *
     * @param id the id of the cooplocalDTO to save.
     * @param cooplocalDTO the cooplocalDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated cooplocalDTO,
     * or with status {@code 400 (Bad Request)} if the cooplocalDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the cooplocalDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/cooplocals/{id}")
    public ResponseEntity<CooplocalDTO> updateCooplocal(
        @PathVariable(value = "id", required = false) final String id,
        @Valid @RequestBody CooplocalDTO cooplocalDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Cooplocal : {}, {}", id, cooplocalDTO);
        if (cooplocalDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, cooplocalDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!cooplocalRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        CooplocalDTO result = cooplocalService.save(cooplocalDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, cooplocalDTO.getId()))
            .body(result);
    }

    /**
     * {@code PATCH  /cooplocals/:id} : Partial updates given fields of an existing cooplocal, field will ignore if it is null
     *
     * @param id the id of the cooplocalDTO to save.
     * @param cooplocalDTO the cooplocalDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated cooplocalDTO,
     * or with status {@code 400 (Bad Request)} if the cooplocalDTO is not valid,
     * or with status {@code 404 (Not Found)} if the cooplocalDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the cooplocalDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/cooplocals/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<CooplocalDTO> partialUpdateCooplocal(
        @PathVariable(value = "id", required = false) final String id,
        @NotNull @RequestBody CooplocalDTO cooplocalDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Cooplocal partially : {}, {}", id, cooplocalDTO);
        if (cooplocalDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, cooplocalDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!cooplocalRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<CooplocalDTO> result = cooplocalService.partialUpdate(cooplocalDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, cooplocalDTO.getId())
        );
    }

    /**
     * {@code GET  /cooplocals} : get all the cooplocals.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of cooplocals in body.
     */
    @GetMapping("/cooplocals")
    public ResponseEntity<List<CooplocalDTO>> getAllCooplocals(CooplocalCriteria criteria) {
        log.debug("REST request to get Cooplocals by criteria: {}", criteria);
        List<CooplocalDTO> entityList = cooplocalQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
     * {@code GET  /cooplocals/count} : count all the cooplocals.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/cooplocals/count")
    public ResponseEntity<Long> countCooplocals(CooplocalCriteria criteria) {
        log.debug("REST request to count Cooplocals by criteria: {}", criteria);
        return ResponseEntity.ok().body(cooplocalQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /cooplocals/:id} : get the "id" cooplocal.
     *
     * @param id the id of the cooplocalDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the cooplocalDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/cooplocals/{id}")
    public ResponseEntity<CooplocalDTO> getCooplocal(@PathVariable String id) {
        log.debug("REST request to get Cooplocal : {}", id);
        Optional<CooplocalDTO> cooplocalDTO = cooplocalService.findOne(id);
        return ResponseUtil.wrapOrNotFound(cooplocalDTO);
    }

    /**
     * {@code DELETE  /cooplocals/:id} : delete the "id" cooplocal.
     *
     * @param id the id of the cooplocalDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/cooplocals/{id}")
    public ResponseEntity<Void> deleteCooplocal(@PathVariable String id) {
        log.debug("REST request to delete Cooplocal : {}", id);
        cooplocalService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id)).build();
    }
}
