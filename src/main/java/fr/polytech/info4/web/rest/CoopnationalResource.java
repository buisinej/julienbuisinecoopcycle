package fr.polytech.info4.web.rest;

import fr.polytech.info4.repository.CoopnationalRepository;
import fr.polytech.info4.service.CoopnationalQueryService;
import fr.polytech.info4.service.CoopnationalService;
import fr.polytech.info4.service.criteria.CoopnationalCriteria;
import fr.polytech.info4.service.dto.CoopnationalDTO;
import fr.polytech.info4.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link fr.polytech.info4.domain.Coopnational}.
 */
@RestController
@RequestMapping("/api")
public class CoopnationalResource {

    private final Logger log = LoggerFactory.getLogger(CoopnationalResource.class);

    private static final String ENTITY_NAME = "coopnational";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CoopnationalService coopnationalService;

    private final CoopnationalRepository coopnationalRepository;

    private final CoopnationalQueryService coopnationalQueryService;

    public CoopnationalResource(
        CoopnationalService coopnationalService,
        CoopnationalRepository coopnationalRepository,
        CoopnationalQueryService coopnationalQueryService
    ) {
        this.coopnationalService = coopnationalService;
        this.coopnationalRepository = coopnationalRepository;
        this.coopnationalQueryService = coopnationalQueryService;
    }

    /**
     * {@code POST  /coopnationals} : Create a new coopnational.
     *
     * @param coopnationalDTO the coopnationalDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new coopnationalDTO, or with status {@code 400 (Bad Request)} if the coopnational has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/coopnationals")
    public ResponseEntity<CoopnationalDTO> createCoopnational(@Valid @RequestBody CoopnationalDTO coopnationalDTO)
        throws URISyntaxException {
        log.debug("REST request to save Coopnational : {}", coopnationalDTO);
        if (coopnationalDTO.getId() != null) {
            throw new BadRequestAlertException("A new coopnational cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CoopnationalDTO result = coopnationalService.save(coopnationalDTO);
        return ResponseEntity
            .created(new URI("/api/coopnationals/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId()))
            .body(result);
    }

    /**
     * {@code PUT  /coopnationals/:id} : Updates an existing coopnational.
     *
     * @param id the id of the coopnationalDTO to save.
     * @param coopnationalDTO the coopnationalDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated coopnationalDTO,
     * or with status {@code 400 (Bad Request)} if the coopnationalDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the coopnationalDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/coopnationals/{id}")
    public ResponseEntity<CoopnationalDTO> updateCoopnational(
        @PathVariable(value = "id", required = false) final String id,
        @Valid @RequestBody CoopnationalDTO coopnationalDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Coopnational : {}, {}", id, coopnationalDTO);
        if (coopnationalDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, coopnationalDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!coopnationalRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        CoopnationalDTO result = coopnationalService.save(coopnationalDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, coopnationalDTO.getId()))
            .body(result);
    }

    /**
     * {@code PATCH  /coopnationals/:id} : Partial updates given fields of an existing coopnational, field will ignore if it is null
     *
     * @param id the id of the coopnationalDTO to save.
     * @param coopnationalDTO the coopnationalDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated coopnationalDTO,
     * or with status {@code 400 (Bad Request)} if the coopnationalDTO is not valid,
     * or with status {@code 404 (Not Found)} if the coopnationalDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the coopnationalDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/coopnationals/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<CoopnationalDTO> partialUpdateCoopnational(
        @PathVariable(value = "id", required = false) final String id,
        @NotNull @RequestBody CoopnationalDTO coopnationalDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Coopnational partially : {}, {}", id, coopnationalDTO);
        if (coopnationalDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, coopnationalDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!coopnationalRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<CoopnationalDTO> result = coopnationalService.partialUpdate(coopnationalDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, coopnationalDTO.getId())
        );
    }

    /**
     * {@code GET  /coopnationals} : get all the coopnationals.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of coopnationals in body.
     */
    @GetMapping("/coopnationals")
    public ResponseEntity<List<CoopnationalDTO>> getAllCoopnationals(CoopnationalCriteria criteria) {
        log.debug("REST request to get Coopnationals by criteria: {}", criteria);
        List<CoopnationalDTO> entityList = coopnationalQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
     * {@code GET  /coopnationals/count} : count all the coopnationals.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/coopnationals/count")
    public ResponseEntity<Long> countCoopnationals(CoopnationalCriteria criteria) {
        log.debug("REST request to count Coopnationals by criteria: {}", criteria);
        return ResponseEntity.ok().body(coopnationalQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /coopnationals/:id} : get the "id" coopnational.
     *
     * @param id the id of the coopnationalDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the coopnationalDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/coopnationals/{id}")
    public ResponseEntity<CoopnationalDTO> getCoopnational(@PathVariable String id) {
        log.debug("REST request to get Coopnational : {}", id);
        Optional<CoopnationalDTO> coopnationalDTO = coopnationalService.findOne(id);
        return ResponseUtil.wrapOrNotFound(coopnationalDTO);
    }

    /**
     * {@code DELETE  /coopnationals/:id} : delete the "id" coopnational.
     *
     * @param id the id of the coopnationalDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/coopnationals/{id}")
    public ResponseEntity<Void> deleteCoopnational(@PathVariable String id) {
        log.debug("REST request to delete Coopnational : {}", id);
        coopnationalService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id)).build();
    }
}
