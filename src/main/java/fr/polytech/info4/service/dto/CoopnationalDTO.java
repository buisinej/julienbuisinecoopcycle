package fr.polytech.info4.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link fr.polytech.info4.domain.Coopnational} entity.
 */
public class CoopnationalDTO implements Serializable {

    @NotNull
    private String id;

    @NotNull
    private String name;

    private CooplocalDTO cooplocal;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CooplocalDTO getCooplocal() {
        return cooplocal;
    }

    public void setCooplocal(CooplocalDTO cooplocal) {
        this.cooplocal = cooplocal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CoopnationalDTO)) {
            return false;
        }

        CoopnationalDTO coopnationalDTO = (CoopnationalDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, coopnationalDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CoopnationalDTO{" +
            "id='" + getId() + "'" +
            ", name='" + getName() + "'" +
            ", cooplocal='" + getCooplocal() + "'" +
            "}";
    }
}
