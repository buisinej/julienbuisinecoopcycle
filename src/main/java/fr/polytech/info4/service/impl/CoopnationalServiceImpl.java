package fr.polytech.info4.service.impl;

import fr.polytech.info4.domain.Coopnational;
import fr.polytech.info4.repository.CoopnationalRepository;
import fr.polytech.info4.service.CoopnationalService;
import fr.polytech.info4.service.dto.CoopnationalDTO;
import fr.polytech.info4.service.mapper.CoopnationalMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Coopnational}.
 */
@Service
@Transactional
public class CoopnationalServiceImpl implements CoopnationalService {

    private final Logger log = LoggerFactory.getLogger(CoopnationalServiceImpl.class);

    private final CoopnationalRepository coopnationalRepository;

    private final CoopnationalMapper coopnationalMapper;

    public CoopnationalServiceImpl(CoopnationalRepository coopnationalRepository, CoopnationalMapper coopnationalMapper) {
        this.coopnationalRepository = coopnationalRepository;
        this.coopnationalMapper = coopnationalMapper;
    }

    @Override
    public CoopnationalDTO save(CoopnationalDTO coopnationalDTO) {
        log.debug("Request to save Coopnational : {}", coopnationalDTO);
        Coopnational coopnational = coopnationalMapper.toEntity(coopnationalDTO);
        coopnational = coopnationalRepository.save(coopnational);
        return coopnationalMapper.toDto(coopnational);
    }

    @Override
    public Optional<CoopnationalDTO> partialUpdate(CoopnationalDTO coopnationalDTO) {
        log.debug("Request to partially update Coopnational : {}", coopnationalDTO);

        return coopnationalRepository
            .findById(coopnationalDTO.getId())
            .map(
                existingCoopnational -> {
                    coopnationalMapper.partialUpdate(existingCoopnational, coopnationalDTO);
                    return existingCoopnational;
                }
            )
            .map(coopnationalRepository::save)
            .map(coopnationalMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<CoopnationalDTO> findAll() {
        log.debug("Request to get all Coopnationals");
        return coopnationalRepository.findAll().stream().map(coopnationalMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<CoopnationalDTO> findOne(String id) {
        log.debug("Request to get Coopnational : {}", id);
        return coopnationalRepository.findById(id).map(coopnationalMapper::toDto);
    }

    @Override
    public void delete(String id) {
        log.debug("Request to delete Coopnational : {}", id);
        coopnationalRepository.deleteById(id);
    }
}
