package fr.polytech.info4.service.impl;

import fr.polytech.info4.domain.Cooplocal;
import fr.polytech.info4.repository.CooplocalRepository;
import fr.polytech.info4.service.CooplocalService;
import fr.polytech.info4.service.dto.CooplocalDTO;
import fr.polytech.info4.service.mapper.CooplocalMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Cooplocal}.
 */
@Service
@Transactional
public class CooplocalServiceImpl implements CooplocalService {

    private final Logger log = LoggerFactory.getLogger(CooplocalServiceImpl.class);

    private final CooplocalRepository cooplocalRepository;

    private final CooplocalMapper cooplocalMapper;

    public CooplocalServiceImpl(CooplocalRepository cooplocalRepository, CooplocalMapper cooplocalMapper) {
        this.cooplocalRepository = cooplocalRepository;
        this.cooplocalMapper = cooplocalMapper;
    }

    @Override
    public CooplocalDTO save(CooplocalDTO cooplocalDTO) {
        log.debug("Request to save Cooplocal : {}", cooplocalDTO);
        Cooplocal cooplocal = cooplocalMapper.toEntity(cooplocalDTO);
        cooplocal = cooplocalRepository.save(cooplocal);
        return cooplocalMapper.toDto(cooplocal);
    }

    @Override
    public Optional<CooplocalDTO> partialUpdate(CooplocalDTO cooplocalDTO) {
        log.debug("Request to partially update Cooplocal : {}", cooplocalDTO);

        return cooplocalRepository
            .findById(cooplocalDTO.getId())
            .map(
                existingCooplocal -> {
                    cooplocalMapper.partialUpdate(existingCooplocal, cooplocalDTO);
                    return existingCooplocal;
                }
            )
            .map(cooplocalRepository::save)
            .map(cooplocalMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<CooplocalDTO> findAll() {
        log.debug("Request to get all Cooplocals");
        return cooplocalRepository.findAll().stream().map(cooplocalMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<CooplocalDTO> findOne(String id) {
        log.debug("Request to get Cooplocal : {}", id);
        return cooplocalRepository.findById(id).map(cooplocalMapper::toDto);
    }

    @Override
    public void delete(String id) {
        log.debug("Request to delete Cooplocal : {}", id);
        cooplocalRepository.deleteById(id);
    }
}
