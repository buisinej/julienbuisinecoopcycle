package fr.polytech.info4.service.mapper;

import fr.polytech.info4.domain.*;
import fr.polytech.info4.service.dto.PlatDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Plat} and its DTO {@link PlatDTO}.
 */
@Mapper(componentModel = "spring", uses = { RestaurantMapper.class })
public interface PlatMapper extends EntityMapper<PlatDTO, Plat> {
    @Mapping(target = "restaurant", source = "restaurant", qualifiedByName = "id")
    PlatDTO toDto(Plat s);

    @Named("id")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    PlatDTO toDtoId(Plat plat);
}
