package fr.polytech.info4.service.mapper;

import fr.polytech.info4.domain.*;
import fr.polytech.info4.service.dto.CoopnationalDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Coopnational} and its DTO {@link CoopnationalDTO}.
 */
@Mapper(componentModel = "spring", uses = { CooplocalMapper.class })
public interface CoopnationalMapper extends EntityMapper<CoopnationalDTO, Coopnational> {
    @Mapping(target = "cooplocal", source = "cooplocal", qualifiedByName = "id")
    CoopnationalDTO toDto(Coopnational s);
}
